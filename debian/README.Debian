fonts-lexi-saebom for Debian
============================

GPL and font embedding
----------------------

These fonts are under the (pure) GNU General Public License, with no
exception about embedding in a document. So you should take care
about the license influence when your non-GPLed document embeds one of
these fonts.

The upstream seemed to stop its font design business. So it's not possible to
change the license.

For your information about this issue:

* The font embedding exception which FSF suggests:
  http://www.fsf.org/licensing/licenses/gpl-faq.html#FontException
* Fedora wiki page about font licensing:
  http://fedoraproject.org/wiki/SIGs/Fonts/Legal

On patent claims by Lexitech
----------------------------

You may found the old claim that this font had Lexitech's patented
technology. But that patent is now invalid in the result of a patent
lawsuit.

Details:

At the release of Lexi Gulim in 2009, the font creator Lexitech
claimed that this font contained their own patented technology,
Actually the patent was quite obvious. It was on how to determine
which Hangul glyph was narrower or wider than others. So all Korean
variable width fonts couldn't avoid this patent (though most Korean
fonts are fixed-width ones).

GPL, the license of this font, allowed us royalty-free license of this
patent anyway. But I thought this patent was a serious threat to the
free/open-source Korean fonts. And Debian packaging has been delayed
to wait for the result of ongoing patent lawsuit. Lexitech finally
lost the lawsuit in 2012 and the patent claims are now invalid.

(Below in Korean)

특허에 대해

어디선가 이 렉시굴림 폰트에 렉시테크 측의 특허가 사용되었다는 글을
봤을 수도 있습니다. 하지만 결론적으로 그 특허는 특허 무효 소송의 결과
무효로 판결되었습니다.

자세히:

2009년 렉시굴림 발표 당시에 제작사인 렉시테크 측은 이 글꼴에 사용된
"가변폭 완성형 한글 글꼴" 기술에 대해 특허를 갖고 있다고 주장했습니다.
실제로 특허의 내용은 어떤 한글 음절이 폭이 넓고 좁은지를 한글 음절의
모음에 따라 판단하는 너무 명백하고 진보성이 부족한 내용이었습니다.
어떤 가변폭 한글 글꼴도 피할 수 없는 내용입니다.

이 특허의 존재 유무와 관계없이 렉시굴림 사용자는 GPL에 따라 렉시테크
측의 특허를 무료 로열티로 사용할 수 있었지만, 이 특허가 허용된다면
모든 자유/오픈소스 한글 폰트에 대한 심각한 문제로 생각했기에 특허
소송이 결론이 난 다음에 데비안에 탑재되기를 바랐습니다. 특허 소송은
2012년에야 최종 판결이 났고 관련 항목은 모두 무효가 되었습니다.


 -- Changwoo Ryu <cwryu@debian.org>, Fri,  3 Jan 2020 21:59:38 +0900
